import javax.imageio.ImageIO;
import javax.swing.JApplet;






import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

public class HappyFaceApplet extends JApplet {
	
	private static final long serialVersionUID = 7082466167601801149L;
	BufferedImage img;
	public void init() {
		try{
		 //URL url = new URL(getCodeBase(), "../examples/strawberry.jpg");
			 URL url = new URL(getCodeBase(), "http://2.bp.blogspot.com/-RsJyf_w5sqY/UCJTRVIY2iI/AAAAAAAAAbk/mVfjMxoes_A/s1600/di-logo-java-orange.png");
			 //URL url = new URL(getCodeBase(), "examples/strawberry.jpg");

			img= ImageIO.read(url);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Cannot open the image!");
		}
	}
	public void paint(Graphics g) {
		String hello = "HELLO Class!";
		g.drawString(hello, 100, 100);
		g.drawLine(0, 0, 150, 150);
		g.drawOval(150, 150, 500, 500);
		g.drawImage(img, 200, 200, null);
	}
}